'''Holds the configurations of the script'''

TRELLO_API_KEY = 'c24c9b1e2146176593d6369480036539'
TRELLO_API_TOKEN = 'ef848b582be858785f6cfc8db22d2d7f30ddfe13e7d0d00d0a7e8cad4eec99c6'
TRELLO_API_EXPIRATION = 'never'
TRELLO_APP_NAME = 'Create Trello Card'

'''Trello board and list to check for cards posted by zapier'''
TRELLO_EMAIL_BOARD = ''
TRELLO_UNSORTED_LIST = ''

'''Trello list to filter the cards'''
TRELLO_IGNORE_LIST = ''
TRELLO_READ_LATER_LIST = ''

'''Trello board to match the client names'''
TRELLO_PROJECT_BOARD = ''

'''Time [in minutes] to refresh the script to check for new trello cards posted by zapier'''
REFRESH_INTERVAL = 5
