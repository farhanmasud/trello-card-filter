"""
-- Zapier is used to create trello cards using the emails from PPH [Workstream message notifications]
-- This script filters those cards into two different lists [read later and ignore]
-- This filter is based on the client name who sends the workstream message 
   And matches it up with another Trello board [Project Board]
-- If that client name is found on that board then the card is moved to read later list
   otherwise to the ignore list
"""

import time
import sys
from trello.util import create_oauth_token
from trello import TrelloClient
import logging

from config_file import (
    TRELLO_API_KEY,
    TRELLO_API_TOKEN,
    TRELLO_API_EXPIRATION,
    TRELLO_APP_NAME,
    TRELLO_EMAIL_BOARD,
    TRELLO_PROJECT_BOARD,
    TRELLO_UNSORTED_LIST,
    TRELLO_IGNORE_LIST,
    TRELLO_READ_LATER_LIST,
    REFRESH_INTERVAL,
)

logging.basicConfig(filename="logfile.log",
                    level=logging.INFO,
                    format="%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S")
logging.getLogger().addHandler(logging.StreamHandler())



def error_handling():
    """Function for checking error and error line"""
    return f"Error: {sys.exc_info()[0]}. {sys.exc_info()[1]}, line: {sys.exc_info()[2].tb_lineno}"


def exit_and_error(text, err):
    """ Handle errors and display the information, exits the program """
    logging.error(text)
    if err:
        logging.error(err)
    try:
        logging.error(error_handling())
    except Exception as e:
        pass # not an error
    logging.warning("Exiting..")
    return sys.exit()


def authorize_trello():
    """Authorize trello using oauth"""

    logging.info("Authorizing Trello..")

    def authorize_trello_first_time():
        try:
            """Request token with the API key"""
            token = create_oauth_token(
                key=TRELLO_API_KEY,
                secret=TRELLO_API_TOKEN,
                expiration=TRELLO_API_EXPIRATION,
                name=TRELLO_APP_NAME,
            )

        except Exception as e:
            exit_and_error("Couldn't authorize Trello", e)

        try:
            with open("trello_client_token.txt", "w") as writer:
                writer.writelines(token["oauth_token"])
                writer.writelines("\n")
                writer.writelines(token["oauth_token_secret"])

        except Exception:
            logging.error("Couldn't write the credentials to to file")

        if not token["oauth_token"]:
            exit_and_error("No token returned", False)
        
        return token["oauth_token"]

    user_token = None
    with open("trello_client_token.txt", "r") as reader:
        file_content = reader.read()

        if file_content:
            user_token = file_content.split("\n")[0]
        else:
            user_token = authorize_trello_first_time()

    if not user_token:
        exit_and_error("Token not found", False)

    else:
        trello_client = TrelloClient(api_key=TRELLO_API_KEY, token=user_token)

        logging.info("Trello Authorized")

        return trello_client


def find_trello_board(trello_client, board_name):
    """check if the board exists"""
    """exit if the board doesn't exist"""

    logging.info(f"Finding board: {board_name}")

    all_boards = trello_client.list_boards()

    concerned_board = None
    for board in all_boards:
        cur_board_name = board.name
        if board_name.strip().lower() == cur_board_name.strip().lower():
            concerned_board = board
            logging.info(f"Board found: {board_name}")
            break

    if not concerned_board:
        logging.info(f"Board NOT found: {board_name}")
        sys.exit()

    return concerned_board


def find_trello_list(concerned_board, list_name):
    """check if the list exists under the board"""
    """exit if the list doesn't exist under the board"""

    logging.info(f"Finding list: {list_name}")
    trello_board_name = concerned_board.name
    all_lists = concerned_board.list_lists()

    concerned_list = None

    for trello_list in all_lists:
        cur_list_name = trello_list.name
        if list_name.strip().lower() == cur_list_name.strip().lower():
            concerned_list = trello_list
            logging.info(
                f"List: {list_name} found under board: {trello_board_name}"
            )
            break

    if not concerned_list:
        logging.info(
            f"List: {list_name} NOT found under board: {trello_board_name}"
        )
        sys.exit()

    return concerned_list


def get_all_cards_titles_as_string(concerned_board):
    """Gets all the titles fo the cards under the concerned board
    and returns a string containing all the titles for matching later"""

    all_cards_titles_string = ""

    all_lists = concerned_board.list_lists()
    for trello_list in all_lists:
        all_cards = trello_list.list_cards()
        for card in all_cards:
            card_name = card.name
            all_cards_titles_string = f"{all_cards_titles_string}{card_name} "

    return all_cards_titles_string.split(" ")


def split_description(description):
    split_location_reg = description.find("Reply to")
    split_location_cap = description.find("REPLY TO")

    split_location = (
        split_location_reg
        if split_location_reg > split_location_cap
        else split_location_cap
    )

    if split_location > 0:
        description = description[:split_location]
    else:
        logging.error("Couldn't find the position to split")

    return description


def filter_cards(
    concerned_unsorted_list,
    concerned_read_later_list,
    concerned_ignore_list,
    concerned_project_board,
):
    """Filters the cards into desired list based on client name match on project board"""

    all_cards_titles_string = get_all_cards_titles_as_string(concerned_project_board)

    read_later_list_id = concerned_read_later_list.id
    ignore_list_id = concerned_ignore_list.id

    try:
        for card in concerned_unsorted_list.list_cards():
            card_name = card.name

            from_pos = card_name.find("from ")
            if from_pos > 0:
                client_name = card_name[from_pos + 5 :]
            else:
                client_name = "No client"

            logging.info(
                f"Processing card containing message from {client_name}"
            )

            if client_name.find(" ") > 0:
                client_name = client_name[: client_name.find(" ")]

            if client_name in all_cards_titles_string:
                logging.info(
                    f"Client name: {client_name} found on the board: {concerned_project_board.name}"
                    
                )
                logging.info(
                    f"Moving card to list: {concerned_read_later_list.name}"
                )
                description = split_description(card.description)
                card.set_description(description)
                card.change_list(read_later_list_id)

            else:
                logging.info(
                    f"Client name: {client_name} wasn't found on the board: {concerned_project_board.name}"
                    
                )
                logging.info(
                    f"Moving card to list: {concerned_ignore_list.name}"
                )
                card.change_list(ignore_list_id)

    except Exception:
        logging.error(error_handling())


def main():
    trello_client = authorize_trello()

    """Retrieving all the necessary boards and lists"""
    email_board = find_trello_board(
        trello_client=trello_client, board_name=TRELLO_EMAIL_BOARD
    )

    project_board = find_trello_board(
        trello_client=trello_client, board_name=TRELLO_PROJECT_BOARD
    )

    unsorted_list = find_trello_list(
        concerned_board=email_board, list_name=TRELLO_UNSORTED_LIST
    )

    read_later_list = find_trello_list(
        concerned_board=email_board, list_name=TRELLO_READ_LATER_LIST
    )

    ignore_list = find_trello_list(
        concerned_board=email_board, list_name=TRELLO_IGNORE_LIST
    )

    while True:
        filter_cards(
            concerned_unsorted_list=unsorted_list,
            concerned_read_later_list=read_later_list,
            concerned_ignore_list=ignore_list,
            concerned_project_board=project_board,
        )

        logging.info(
            f"The script will refresh to check and filter new cards in {REFRESH_INTERVAL} minutes"
        )

        time.sleep(60 * REFRESH_INTERVAL)


if __name__ == "__main__":
    main()
